package _02.odev.builder.bani;

public class AsciiConverter extends TextConverter{
	
	private AsciiText at;
	
	@Override
	public void convertCharacter(char c) {
		
	}

	@Override
	public void convertParagraph(String paragraf) {
		int i=0;
		while(i<paragraf.length()) {
			at.append(paragraf.charAt(i));
			i++;
		}
		getResult();
	}
	
	public AsciiText getResult() {
		return at;
	}
	
	
	
}
