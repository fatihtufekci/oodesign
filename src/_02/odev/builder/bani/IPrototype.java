package _02.odev.builder.bani;

public interface IPrototype {
	
	Object clone() throws CloneNotSupportedException;
	
}
