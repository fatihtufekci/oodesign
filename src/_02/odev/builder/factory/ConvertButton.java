package _02.odev.builder.factory;

import javax.swing.JButton;

public class ConvertButton extends JButton implements IComponent{
	
	public ConvertButton() {
		setText("Convert");
	}
	
}
