package _02.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {

	public static void main(String[] args) {
		
		Person p = new Person("Fatih");
		// System.out.println(p.getClass().getName());
		// System.out.println(p.name); // error, not accessible
		
		try {
			Field field = p.getClass().getDeclaredField("name");
			field.setAccessible(true);
			System.out.println(field.get(p));
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//
		//
		
		
		// System.out.println(Person.numPeople); // error, not accessible
		
		try {
			
			Field field = Person.class.getDeclaredField("numPeople");
			field.setAccessible(true);
			System.out.println(field.get(null));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//
		//
		
		
		// System.out.println(p.getName());
		try {
			Method method = p.getClass().getDeclaredMethod("getName");
			System.out.println(method.invoke(p));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//
		//
		
		// p.setName("Yunus");
		try {
			Method method = p.getClass().getDeclaredMethod("setName", String.class);
			method.invoke(p, "Yusuf");
			System.out.println(p.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//
		//
		
		
		//Person.printPerson(p);
		try {
			Method method = Person.class.getDeclaredMethod("printPerson", Person.class);
			method.invoke(null, p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//
		//
		
		
		
//		Person other = new Person("Ali");
//		Person.printPerson(other);
		Person other;
		try {
			Constructor<Person> constructor = Person.class.getDeclaredConstructor(new Class[] {String.class});
			other = constructor.newInstance("Ali");
			Person.printPerson(other);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

}
