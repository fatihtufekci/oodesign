package _02.factory;

import javax.swing.JButton;

public class ButtonFactory {
	
	private static ButtonFactory instance;
	private JButton button;
	
	private ButtonFactory() { }
	
	public static ButtonFactory getInstance() {
		if(instance == null) {
			synchronized (ButtonFactory.class) {
				if(instance==null) {
					instance = new ButtonFactory();
				}
			}
		}
		return instance;
	}
	
	public JButton createButton(String buttonName) {
		
		if(buttonName.equals("save")) {
			button = new SaveButton();
		}else if(buttonName.equals("delete")) {
			button = new DeleteButton();
		}else if(buttonName.equals("update")) {
			button = new UpdateButton();
		}
		
		return button;
	}
	
	public JButton createButtonWithInterface(IComponent buttonClass) {
		if(buttonClass instanceof SaveButton) {
			button = new SaveButton();
		}else if(buttonClass instanceof DeleteButton) {
			button = new DeleteButton();
		}else if(buttonClass instanceof UpdateButton) {
			button = new UpdateButton();
		}
		return button;
	}
	
	
}
