package _02.odev.builder.bani;


public abstract class TextConverter implements IPrototype{
	
	private AsciiConverter ac=null;
	
	public TextConverter() { }
	
	public AsciiConverter getAscii() {
		if(ac==null) {
			ac = new AsciiConverter();
		}
		return ac;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public abstract void convertCharacter(char c);
	
	public abstract void convertParagraph(String paragraf);
	
}
