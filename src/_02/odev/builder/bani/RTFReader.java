package _02.odev.builder.bani;

public class RTFReader {
	
	private TextConverter builder;
	public RTFReader(TextConverter builder) {
		this.builder = builder;
	}
	
	public String parseRTF(Document doc) {
		String veri = "";
		try {
			String s = doc.readFileAsString();
			System.out.println(s);
			builder.convertParagraph(s);
			veri = builder.getAscii().getResult().getVeri();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return veri;
	}
	
}
