package _02.odev.builder;

import java.io.FileReader;
import java.io.IOException;

public class Deneme {
	
	private A at;
	
	public Deneme(A at) {
		this.at = at;
	}
	
	public static void main(String[] args) {
		
		Deneme d = new Deneme(new A());
		d.deneme("Fatih");
		System.out.println(d.getResult().getVeri());
	}
	
	public void deneme(String paragraf) {
		int i=0;
		while(i<paragraf.length()) {
			at.append(paragraf.charAt(i));
			i++;
		}
	}
	
	public A getResult() {
		return at;
	}
	
}

class A{
	private String veri;
	
	public String getVeri() {
		return veri;
	}
	
	public void append(char d) {
		veri.concat(""+(int)d);
	}
}
