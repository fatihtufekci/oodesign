package _02.odev;

public class Istanbul{
	
	private static Istanbul instance;
	
	private Istanbul() { }
	
	public static Istanbul getInstance() {
		
		if(instance == null) {
			synchronized (Istanbul.class) {
				if(instance == null) {
					instance = new Istanbul();
				}
			}
		}
		return instance;
	}
	
	public void birSeyYap(int sayi) {
		for (int i = 0; i < sayi; i++) {
			System.out.println("Fatih");
		}
	}
	
}
