package _02.factory;

import javax.swing.JButton;

public class SaveButton extends JButton implements IComponent{
	
	public SaveButton() {
		setText("Kaydet");
		ButtonFactoryReflection.getInstance().registerButton("save", SaveButton.class);
	}
	
}
