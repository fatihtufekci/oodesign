package _02.factory;

import java.lang.reflect.Constructor;
import java.util.HashMap;

import javax.swing.JButton;

public class ButtonFactoryReflection {
	
	private static ButtonFactoryReflection instance;
	private static HashMap<String, Class<?>> mapProducts = new HashMap<>();
	
	private ButtonFactoryReflection() { }
	
	public static ButtonFactoryReflection getInstance() {
		if(instance==null) {
			synchronized (ButtonFactoryReflection.class) {
				if(instance==null) {
					instance = new ButtonFactoryReflection();
				}
			}
		}
		return instance;
	}
	
	public void registerButton(String productId, Class<? extends JButton> productClass) {
		mapProducts.put(productId, productClass);
	}
	
	public JButton createButton(String productId) {
		
		try {
			
			Class<?> productClass = (Class<?>) mapProducts.get(productId);
			System.out.println(mapProducts.get(productId));
			Constructor<?> productConstructor = productClass.getDeclaredConstructor(new Class[] {});
			return (JButton) productConstructor.newInstance(new Object[] {});
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public JButton createButton2(Class<? extends JButton> productClass) {
		try {
			
			// Class<?> productClass = (Class<?>)sinif.getClass();
			//productClass.newInstance();
			Constructor<?> productConstructor = productClass.getDeclaredConstructor(new Class[] {});
			return (JButton) productConstructor.newInstance(new Object[] {});
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
