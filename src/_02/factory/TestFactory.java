package _02.factory;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TestFactory {

	public static void main(String[] args) {

		JFrame frame = new JFrame("FrameTest");
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JLabel label = new JLabel("BUNLAR BUTTONDUR");

		JButton button = new JButton();
		button.setText("Press me");
		
//		JButton saveI = ButtonFactory.getInstance().createButtonWithInterface(new SaveButton());
		
		// �F �LE
//		JButton save = ButtonFactory.getInstance().createButton("save");
//		JButton delete = ButtonFactory.getInstance().createButton("delete");
//		JButton update = ButtonFactory.getInstance().createButton("update");

		// REFLECT�ON �LE
//		ButtonFactoryReflection.getInstance().registerButton("save", SaveButton.class);
//		JButton save = ButtonFactoryReflection.getInstance().createButton("save");
		
		JButton save = ButtonFactoryReflection.getInstance().createButton2(SaveButton.class);
		
		panel.add(label);
		panel.add(save);

		frame.add(panel);
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
