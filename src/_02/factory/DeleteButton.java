package _02.factory;

import javax.swing.JButton;

public class DeleteButton extends JButton implements IComponent {
	
	public DeleteButton() {
		setText("Sil");
		ButtonFactoryReflection.getInstance().registerButton("delete", DeleteButton.class);
	}
	
}
