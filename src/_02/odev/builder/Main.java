package _02.odev.builder;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

import _02.odev.builder.bani.AsciiConverter;
import _02.odev.builder.bani.Document;
import _02.odev.builder.bani.RTFReader;
import _02.odev.builder.factory.ButtonFactory;
import _02.odev.builder.factory.CloneButton;
import _02.odev.builder.factory.ConvertButton;
import _02.odev.builder.factory.DosyaAcButton;
import _02.odev.builder.factory.KaydetButton;

public class Main {
	
	private static String dosyaAdi;
	private static String asciiDeger;
	
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("FrameTest");
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JButton dosyaAc = ButtonFactory.getInstance().createButton(DosyaAcButton.class);
		JButton kaydet = ButtonFactory.getInstance().createButton(KaydetButton.class);
		JButton clone = ButtonFactory.getInstance().createButton(CloneButton.class);
		JButton convert = ButtonFactory.getInstance().createButton(ConvertButton.class);
		
		dosyaAc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("C:\\Users\\Lenovo\\Desktop"));
				chooser.setDialogTitle("Select File");
				int returnVal = chooser.showOpenDialog(null);
				File file = chooser.getSelectedFile();
				
				String fileName = file.getAbsolutePath();
				dosyaAdi = fileName;
			}
		});
		
		String asciiValue = null;
		
		convert.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Document doc = new Document(dosyaAdi);
				RTFReader rtf = new RTFReader(new AsciiConverter());
				String asciiValue = rtf.parseRTF(doc);
			}
		});
		
		
		//asciiDeger = asciiValue;
		
		kaydet.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				File file = new File("C:\\Users\\Lenovo\\Desktop\\ascii.txt");
				if(!file.exists()) {
					try {
						file.createNewFile();
						FileWriter fw = new FileWriter(file);
						BufferedWriter bw = new BufferedWriter(fw);
						bw.write(asciiValue);
						bw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				
			}
		});
		
		clone.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				
				
			}
		});
		
		panel.add(dosyaAc);
		panel.add(kaydet);
		panel.add(clone);
		panel.add(convert);

		frame.add(panel);
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public static String readFileAsString(String fileName) throws Exception {
		String data = "";
		data = new String(Files.readAllBytes(Paths.get(fileName)));
		return data;
	}

}
