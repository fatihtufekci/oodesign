package _02.odev.builder.factory;

import javax.swing.JButton;

public class ButtonFactory {
	
	private static ButtonFactory instance;
	private JButton button;
	
	private ButtonFactory() { }
	
	public static ButtonFactory getInstance() {
		if(instance == null) {
			synchronized(ButtonFactory.class) {
				if(instance==null) {
					return new ButtonFactory();
				}
			}
		}
		return instance;
	}
	
	public JButton createButton(Class<? extends JButton> butonComponent) {
		try {
			return (JButton)butonComponent.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
}
