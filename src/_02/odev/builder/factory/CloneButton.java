package _02.odev.builder.factory;

import javax.swing.JButton;

public class CloneButton extends JButton implements IComponent{
	
	public CloneButton() {
		setText("Clone");
	}
	
}
