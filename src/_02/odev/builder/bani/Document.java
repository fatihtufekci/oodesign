package _02.odev.builder.bani;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Document {
		
	private String fileName;
	
	public Document() { }
	
	public Document(String fileName) {
		this.fileName = fileName;
	}
	
	public char getNextToken() throws IOException {
		FileReader fr = new FileReader(fileName);
		int i; 
	    while ((i=fr.read()) != -1) 
	      System.out.print((char) i);
		return 0;
	}
	
	public String readFile() {
		BufferedReader br=null;
		String result = null;
		try {
			br = new BufferedReader(new FileReader(fileName));
			String line;
			while((line = br.readLine())!=null) {
				result.concat(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public String readFileAsString() throws Exception {
		return new String(Files.readAllBytes(Paths.get(fileName)));
	}
}
