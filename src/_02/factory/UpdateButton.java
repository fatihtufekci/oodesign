package _02.factory;

import javax.swing.JButton;

public class UpdateButton extends JButton implements IComponent {
	
	public UpdateButton() {
		setText("G�ncelle");
		ButtonFactoryReflection.getInstance().registerButton("update", UpdateButton.class);
	}
	
}
